/**
 * React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react'
import {
  Dimensions,
  StatusBar
} from 'react-native'
import 'react-native-gesture-handler'
import { SafeAreaProvider } from 'react-native-safe-area-context'

import AppNavigator from './src/navigation/AppNavigator'
import EStyleSheet from 'react-native-extended-stylesheet'

class App extends Component {
  render() {
    const entireScreenWidth = Dimensions.get('window').width
    EStyleSheet.build({ $rem: entireScreenWidth / 320 })

    return (
      <SafeAreaProvider>
        <StatusBar
          backgroundColor='rgba(0, 0, 0, .005)'
          barStyle='dark-content'
        />
        <AppNavigator />
      </SafeAreaProvider>
    )
  }
}

export default App