export default {
  customStyle: (baseStyle, customStyle) => {
    return Array.isArray(customStyle)
      ? [baseStyle, ...customStyle]
      : [baseStyle, customStyle]
  }
}