export default {
  Colors: {
    Black: '#2D2D2D',
    White: '#FFFFFF',
    Silver: '#EFEFEF',
    DarkMediumGray: '#A8A8A8',
    SpanishGray: '#9E9E9E',
    RedRuber: '#E14575',
    DarkPink: '#E55070',
    RedCherry: '#D33464',
    PinkOrange: '#F69465',
    LightPink: '#EA586D'
  }
}
