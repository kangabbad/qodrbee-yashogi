import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  TouchableHighlight,
  View,
  Text
} from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

import {
  Theme,
  Utils
} from '../../../constants'

class BottomTabItem extends Component {
  render() {
    const { containerStyle } = this.props

    return (
      <TouchableHighlight
        underlayColor='rgba(0, 0, 0, .05)'
        onPress={this.props.onPress}
        style={Utils.customStyle(styles['container'], containerStyle)}
      >
        {this.content()}
      </TouchableHighlight>
    )
  }

  content = () => {
    const { contentStyle } = this.props

    return (
      <View style={Utils.customStyle(styles['content'], contentStyle)}>
        {this.icon()}
        {this.title()}
      </View>
    )
  }

  icon = () => this.props.icon

  title = () => {
    const { titleStyle } = this.props

    return (
      <Text style={Utils.customStyle(styles['title'], titleStyle)}>
        {this.props.title}
      </Text>
    )
  }
}

BottomTabItem.propTypes = {
  onPress: PropTypes.func,
  containerStyle: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ]),
  contentStyle: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ]),
  title: PropTypes.string,
  titleStyle: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ]),
  icon: PropTypes.object
}

BottomTabItem.defaultProps = {
  onPress: () => {},
  title: 'Menu',
  icon: <MaterialCommunityIcons name='book-multiple-variant' size={EStyleSheet.value('20rem')} color={Theme.Colors.Black} />
}

export default BottomTabItem

const styles = EStyleSheet.create({
  'container': {
    flex: 1
  },
  'content': {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  'title': {
    fontSize: '8rem',
    color: '#2D2D2D'
  }
})
