import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  View
} from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import EStyleSheet from 'react-native-extended-stylesheet'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import AntD from 'react-native-vector-icons/AntDesign'

import BottomTabItem from './BottomTabItem'
import FloatBottomTabItem from './FloatBottomTabItem'
import { Theme } from '../../../constants'

class BottomTab extends Component {
  render() {
    return (
      <SafeAreaView style={styles['wrapper']}>
        <View style={styles['container']}>
          {this.exploreTab()}
          {this.pointTab()}
          {this.myStoreTab()}
          {this.orderTab()}
          {this.accountTab()}
        </View>
      </SafeAreaView>
    )
  }

  tab = (tab) => {
    const { navigation } = this.props
    const { routes, index } = navigation.state

    const color = index === tab
      ? Theme.Colors.PinkOrange
      : Theme.Colors.Black

    const routeName = routes[tab].routes[0].routeName

    const onPressTab = () => {
      setTimeout(() => {
        this.props.navigation.navigate(routeName)
      }, 0)
    }

    return {
      color,
      onPressTab
    }
  }

  exploreTab = () => {
    return (
      <BottomTabItem
        title='Explore'
        icon={
          <MaterialCommunityIcons
            name='compass-outline'
            size={EStyleSheet.value('20rem')}
            color={this.tab(0).color}
          />
        }
        onPress={this.tab(0).onPressTab}
      />
    )
  }

  pointTab = () => {
    return (
      <BottomTabItem
        title='Point'
        icon={
          <FontAwesome5
            name='coins'
            size={EStyleSheet.value('16rem')}
            color={this.tab(1).color}
          />
        }
        onPress={this.tab(1).onPressTab}
        titleStyle={{ marginTop: EStyleSheet.value('2rem') }}
        containerStyle={{ paddingTop: EStyleSheet.value('1rem') }}
      />
    )
  }

  myStoreTab = () => {
    return (
      <FloatBottomTabItem />
    )
  }

  orderTab = () => {
    return (
      <BottomTabItem
        title='Order'
        icon={
          <FontAwesome5
            name='tasks'
            size={EStyleSheet.value('17rem')}
            color={this.tab(2).color}
          />
        }
        onPress={this.tab(2).onPressTab}
        titleStyle={{ marginTop: EStyleSheet.value('2rem') }}
      />
    )
  }

  accountTab = () => {
    return (
      <BottomTabItem
        title='Akun'
        icon={
          <AntD
            name='user'
            size={EStyleSheet.value('20rem')}
            color={this.tab(3).color}
          />
        }
        onPress={this.tab(3).onPressTab}
      />
    )
  }
}

BottomTab.propTypes = {
  navigation: PropTypes.object
}

export default BottomTab

const styles = EStyleSheet.create({
  'wrapper': {
    paddingTop: '0rem',
    backgroundColor: Theme.Colors.White
  },
  'container': {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'stretch',
    height: '40rem',
    backgroundColor: Theme.Colors.White,
    borderTopWidth: '1rem',
    borderTopColor: Theme.Colors.Silver,
    borderBottomWidth: '1rem',
    borderBottomColor: Theme.Colors.Silver,
  },
  'circle-center': {
    borderWidth: '1rem',
    borderColor: '#2d2d2d',
    position: 'absolute',
    top: '-22rem',
    left: '42.5%',
    right: '0rem',
    zIndex: '-99rem',
    borderRadius: '60rem',
    backgroundColor: '#ffffff',
    height: '48rem',
    width: '48rem'
  }
})
