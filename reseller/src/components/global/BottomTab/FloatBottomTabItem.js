import React, { Component } from 'react'
import {
  Platform,
  View,
  TouchableHighlight,
  Text
} from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import EStyleSheet from 'react-native-extended-stylesheet'

import { Theme } from '../../../constants'

class FloatBottomTabItem extends Component {
  render() {
    return (
      <View style={styles['wrapper']}>
        {this.btn()}
        {this.title()}
      </View>
    )
  }

  btn = () => {
    return (
      <View style={styles['btn__container']}>
        <View style={styles['half-circle']} />
        <TouchableHighlight
          underlayColor={Theme.Colors.RedCherry}
          onPress={() => {}}
          style={styles['btn']}
        >
          <MaterialCommunityIcons
            name='store'
            size={EStyleSheet.value('23rem')}
            color={Theme.Colors.White}
          />
        </TouchableHighlight>
      </View>
    )
  }

  title = () => {
    return (
      <Text style={styles['title']}>
        Toko Saya
      </Text>
    )
  }
}

export default FloatBottomTabItem

const styles = EStyleSheet.create({
  'wrapper': {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  'btn__container': {
    position: 'absolute',
    top: '-25rem',
    borderWidth: '1rem',
    borderColor: Theme.Colors.Silver,
    borderRadius: '60rem',
    backgroundColor: '#ffffff',
    padding: '3rem'
  },
  'btn': {
    borderRadius: '50rem',
    backgroundColor: Theme.Colors.RedRuber,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: Platform.select({ ios: '7rem', android: '8rem' }),
    paddingHorizontal: Platform.select({ ios: '8rem', android: '8rem' })
  },
  'half-circle': {
    position: 'absolute',
    top: '3rem',
    right: '-1rem',
    bottom: '-5rem',
    left: '-1rem',
    borderWidth: '1rem',
    borderTopLeftRadius: '100rem',
    borderTopRightRadius: '100rem',
    borderColor: Theme.Colors.White,
    backgroundColor: Theme.Colors.White,
  },
  'title': {
    position: 'absolute',
    bottom: '4rem',
    fontSize: '8rem',
    color: Theme.Colors.Black
  }
})
