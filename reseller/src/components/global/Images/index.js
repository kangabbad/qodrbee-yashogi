import React from 'react'
import FastImage from 'react-native-fast-image'

const Images = props => (
  <FastImage {...props} />
)

export default Images