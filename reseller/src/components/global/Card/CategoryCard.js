import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Text,
  ImageBackground
} from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'

import { Theme } from '../../../constants'

class CategoryCard extends Component {
  render() {
    return (
      <View style={styles['container']}>
        {this.images()}
      </View>
    )
  }

  images = () => {
    const { images, height, width, title } = this.props

    return (
      <ImageBackground
        source={images}
        resizeMode='cover'
        style={{ height, width }}
      >
        <View style={styles['overlay']}>
          <Text style={styles['title']}>
            {title}
          </Text>
        </View>
      </ImageBackground>
    )
  }
}

CategoryCard.propTypes = {
  images: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object
  ]),
  title: PropTypes.string,
  height: PropTypes.number,
  width: PropTypes.number
}

export default CategoryCard

const styles = EStyleSheet.create({
  'container': {
    borderRadius: '5rem',
    overflow: 'hidden'
  },
  'overlay': {
    alignSelf: 'flex-start',
    width: '100%',
    backgroundColor: 'rgba(0, 0, 0, .5)',
    paddingVertical: '3rem',
    paddingHorizontal: '5rem',
    marginTop: 'auto'
  },
  'title': {
    fontSize: '10rem',
    fontWeight: '700',
    color: Theme.Colors.White
  }
})