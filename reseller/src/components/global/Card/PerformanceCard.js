import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Text
} from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import LinearGradient from 'react-native-linear-gradient'
import FontAwesome from 'react-native-vector-icons/FontAwesome'

import {
  Theme,
  Utils
} from '../../../constants'

class PerformanceCard extends Component {
  render () {
    const { containerStyle } = this.props

    return (
      <View style={Utils.customStyle(styles['container'], containerStyle)}>
        {this.cardIcon()}
        {this.title()}
        {this.amount()}
        {this.percentage()}
      </View>
    )
  }

  cardIcon = () => {
    const { icon } = this.props

    if (icon) {
      return icon
    } else {
      return (
        <LinearGradient
          colors={[
            Theme.Colors.RedRuber,
            Theme.Colors.PinkOrange
          ]}
          style={styles['icon__wrapper']}
        >
          <View style={styles['icon__container']}>
            <FontAwesome
              name='code'
              size={EStyleSheet.value('27rem')}
              color={Theme.Colors.White}
            />
          </View>
        </LinearGradient>
      )
    }
  }

  title = () => {
    const { title } = this.props

    return (
      <Text style={styles['card__title']}>
        {title}
      </Text>
    )
  }

  amount = () => {
    const { amount } = this.props

    return (
      <Text style={styles['card__amount']}>
        Rp {amount}
      </Text>
    )
  }

  percentage = () => {
    const { percentage } = this.props

    return (
      <Text style={styles['card__percentage']}>
        {percentage}%
      </Text>
    )
  }
}

PerformanceCard.propTypes = {
  data: PropTypes.array,
  containerStyle: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array
  ]),
  icon: PropTypes.object,
  title: PropTypes.string,
  amount: PropTypes.string,
  percentage: PropTypes.string
}

export default PerformanceCard

const styles = EStyleSheet.create({
  'container': {
    borderRadius: '5rem',
    backgroundColor: Theme.Colors.White,
    padding: '12rem',
    marginTop: '7rem',
    marginBottom: '13rem',
    marginLeft: '15rem'
  },
  'icon__wrapper': {
    borderRadius: '100rem',
    alignSelf: 'flex-start',
    alignItems: 'center',
    justifyContent: 'center',
    height: '40rem',
    width: '40rem'
  },
  'icon__container': {
    marginBottom: '2rem'
  },
  'card__title': {
    fontSize: '10rem',
    color: Theme.Colors.SpanishGray,
    marginTop: '13rem'
  },
  'card__amount': {
    fontSize: '15rem',
    fontWeight: '700',
    color: Theme.Colors.PinkOrange,
    marginTop: '8rem'
  },
  'card__percentage': {
    fontSize: '10rem',
    color: Theme.Colors.DarkMediumGray,
    marginTop: '4rem'
  }
})
