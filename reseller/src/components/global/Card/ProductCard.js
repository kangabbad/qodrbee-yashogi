import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Text
} from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

import Images from '../Images'
import {
  Theme,
  Utils
} from '../../../constants'

class ProductCard extends Component {
  render() {
    const { containerStyle } = this.props

    return (
      <View style={Utils.customStyle(styles['container'], containerStyle)}>
        {this.images()}
        {this.topContent()}
        {this.bottomContent()}
      </View>
    )
  }

  images = () => {
    const { images } = this.props

    return (
      <Images
        source={images}
        resizeMode='cover'
        style={styles['images']}
      />
    )
  }

  topContent = () => {
    return (
      <View style={styles['desc--top']}>
        {this.price()}
        {this.expressIcon()}
      </View>
    )
  }

  price = () => {
    const { price } = this.props

    return (
      <Text style={styles['price']}>
        <Text style={styles['price--currency']}>
          Rp
        </Text>
        <Text style={styles['price--tag']}>
          &nbsp;{price}
        </Text> 
      </Text>
    )
  }

  expressIcon = () => {
    const { express } = this.props

    if (express) {
      return (
        <MaterialCommunityIcons 
          name='truck-fast'
          size={EStyleSheet.value('11rem')}
          color={Theme.Colors.PinkOrange}
        />
      )
    }
  }

  bottomContent = () => {
    const { soldout } = this.props

    if (soldout) {
      return (
        <Text style={styles['sold']}>
          {soldout}
        </Text>
      )
    }
  }
}

ProductCard.propTypes = {
  containerStyle: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ]),
  images: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object
  ]),
  price: PropTypes.string,
  soldout: PropTypes.number,
  express: PropTypes.bool
}

export default ProductCard

const styles = EStyleSheet.create({
  'container': {
    borderRadius: '3rem',
    overflow: 'hidden',
    backgroundColor: Theme.Colors.White,
    marginRight: '7rem'
  },
  'images': {
    height: '125rem',
    width: '115rem'
  },
  'desc--top': {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: '5rem',
    paddingHorizontal: '7rem'
  },
  'price': {
    color: Theme.Colors.PinkOrange
  },
  'price--currency': {
    fontSize: '6rem'
  },
  'price--tag': {
    fontSize: '8rem',
    fontWeight: '700'
  }
})