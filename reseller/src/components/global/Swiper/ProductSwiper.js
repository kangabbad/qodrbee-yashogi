import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Text,
  TouchableOpacity,
  FlatList
} from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'

import ProductCard from '../Card/ProductCard'
import { Theme } from '../../../constants'

class ProductSwiper extends Component {
  render() {
    const { containerStyle } = this.props

    return (
      <View style={containerStyle}>
        {this.header()}
        {this.list()}
      </View>
    )
  }

  header = () => {
    return (
      <View style={styles['product__header']}>
        {this.headerTitle()}
        {this.headerBtn()}
      </View>
    )
  }

  headerTitle  = () => {
    const { headerTitle } = this.props

    return (
      <Text style={styles['product__header__title']}>
        {headerTitle}
      </Text>
    )
  }

  headerBtn = () => {
    return (
      <TouchableOpacity
        onPress={() => {}}
        style={styles['product__header__btn']}
      >
        <Text style={styles['product__header__btn__title']}>
          Lihat Semua
        </Text>
      </TouchableOpacity>
    )
  }

  list = () => {
    const { data } = this.props

    return (
      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        data={data}
        keyExtractor={(item, index) => item + index.toString()}
        renderItem={this.listItem}
      />
    )
  }

  listItem = ({ item, index }) => {
    return (
      <ProductCard
        key={index}
        images={item.images}
        title={item.name}
        price={item.price}
        express={item.express}
        containerStyle={this.listItemStyle(index)}
      />
    )
  }

  listItemStyle = (index) => {
    if (index === 0) {
      return { marginLeft: EStyleSheet.value('15rem') }
    } else if (this.props.data.length === index + 1) {
      return { marginRight: EStyleSheet.value('15rem') }
    }
  }
}

ProductSwiper.propTypes = {
  containerStyle: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ]),
  data: PropTypes.array,
  headerTitle: PropTypes.string
}

export default ProductSwiper

const styles = EStyleSheet.create({
  'product__header': {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  'product__header__title': {
    fontSize: '14rem',
    fontWeight: '700',
    color: Theme.Colors.PinkOrange,
    paddingHorizontal: '15rem',
    paddingVertical: '10rem'
  },
  'product__header__btn': {
    paddingHorizontal: '15rem',
    paddingVertical: '10rem'
  },
  'product__header__btn__title': {
    fontSize: '10rem',
    color: Theme.Colors.Black
  }
})