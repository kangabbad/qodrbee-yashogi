import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Platform,
  View,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Text
} from 'react-native'
import {
  Header
} from 'react-native-elements'
import { withNavigation } from 'react-navigation'
import EStyleSheet from 'react-native-extended-stylesheet'

import AntD from 'react-native-vector-icons/AntDesign'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

import { Theme } from '../../../constants'

class MainHeader extends Component {
  render() {
    return (
      <Header
        statusBarProps={{
          barStyle: 'light-content',
          backgroundColor: Theme.Colors.RedRuber,
          animated: true
        }}
        backgroundColor={Theme.Colors.RedRuber}
        containerStyle={styles['container']}
        leftContainerStyle={{ flex: 0 }}
        rightContainerStyle={{ flex: 0 }}
      >
        {this.leftMenu()}
        {this.searchBar()}
        {this.rightMenu()}
      </Header>
    )
  }

  leftMenu = () => {
    return (
      <TouchableOpacity
        onPress={() => {}}
      >
        <AntD
          name='message1'
          size={EStyleSheet.value('20rem')}
          color={Theme.Colors.White}
        />
      </TouchableOpacity>
    )
  }

  searchBar = () => {
    return (
      <TouchableWithoutFeedback
        onPress={() => this.props.navigation.navigate('SearchScreen')}
      >
        <View style={styles['searchbar__wrapper']}>
          <MaterialIcons
            name='search'
            size={EStyleSheet.value('18rem')}
            color={Theme.Colors.SpanishGray}
          />
          <Text style={styles['searchbar__title']}>
              Cari...
          </Text>
        </View>
      </TouchableWithoutFeedback>
    )
  }

  rightMenu = () => {
    return (
      <View style={styles['menu--right__wrapper']}>
        <TouchableOpacity
          onPress={() => {}}
        >
          <FontAwesome
            name='address-book-o'
            size={EStyleSheet.value('17rem')}
            color={Theme.Colors.White}
            style={{ marginRight: EStyleSheet.value('8rem') }}
          />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {}}
        >
          <EvilIcons
            name='bell'
            size={EStyleSheet.value('25rem')}
            color={Theme.Colors.White}
          />
        </TouchableOpacity>
      </View>
    )
  }
}

MainHeader.propTypes = {
  navigation: PropTypes.object
}

export default withNavigation(MainHeader)

const styles = EStyleSheet.create({
  'container': {
    borderBottomWidth: '0rem',
    height: 'auto',
    width: '100%',
    paddingRight: '15rem',
    paddingLeft: '15rem',
    paddingTop: Platform.select({
      android: '13rem',
      ios: '40rem'
    }),
    paddingBottom: '12rem'
  },
  'searchbar__wrapper': {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: '3rem',
    width: '90%',
    backgroundColor: Theme.Colors.White,
    paddingVertical: '5rem',
    paddingHorizontal: '7rem'
  },
  'searchbar__title': {
    fontSize: '12rem',
    color: Theme.Colors.DarkMediumGray,
    marginLeft: '5rem'
  },
  'menu--right__wrapper': {
    flexDirection: 'row',
    alignItems: 'center'
  }
})
