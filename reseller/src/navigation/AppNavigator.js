/* eslint-disable react/display-name */
/* eslint-disable react/react-in-jsx-scope */
import React from 'react'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createBottomTabNavigator } from 'react-navigation-tabs'

import ExploreScreen from '../screens/Explore/ExploreScreen'
import DetailsScreen from '../screens/OrderScreen'
import LearnScreen from '../screens/LearnScreen'
import AccountScreen from '../screens/AccountScreen'
import SearchScreen from '../screens/SearchScreen'

import {
  StackDefaultNavigationOptions,
  BottomTabDefaultNavigationOptions
} from './config/NavigationConfig'

import BottomTab from '../components/global/BottomTab/BottomTab'

const ExploreStack = createStackNavigator(
  {
    ExploreScreen,
    SearchScreen
  },
  {
    initialRouteName: 'ExploreScreen',
    defaultNavigationOptions: StackDefaultNavigationOptions
  }
)

const LearnStack = createStackNavigator(
  {
    LearnScreen
  },
  {
    initialRouteName: 'LearnScreen',
    defaultNavigationOptions: StackDefaultNavigationOptions
  }
)

const OrderStack = createStackNavigator(
  {
    DetailsScreen
  },
  {
    initialRouteName: 'DetailsScreen',
    defaultNavigationOptions: StackDefaultNavigationOptions
  }
)

const AccountStack = createStackNavigator(
  {
    AccountScreen
  },
  {
    initialRouteName: 'AccountScreen',
    defaultNavigationOptions: StackDefaultNavigationOptions
  }
)

const AppNavigator = createBottomTabNavigator(
  {
    Explore: ExploreStack,
    Learn: LearnStack,
    Order: OrderStack,
    Akun: AccountStack
  },
  {
    tabBarComponent: props => <BottomTab {...props} />,
    defaultNavigationOptions: BottomTabDefaultNavigationOptions
  }
)

export default createAppContainer(AppNavigator)