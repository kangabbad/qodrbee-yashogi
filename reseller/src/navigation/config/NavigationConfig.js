import { TransitionPresets } from 'react-navigation-stack'
import { Theme } from '../../constants'

export const StackDefaultNavigationOptions = {
  ...TransitionPresets.SlideFromRightIOS,
  headerShown: false
}

export const BottomTabDefaultNavigationOptions = ({ navigation }) => ({
  tabBarVisible: navigation.state.index === 0,
  tabBarOptions: {
    activeTintColor: Theme.Colors.PinkOrange,
    inactiveTintColor: Theme.Colors.Black
  }
})
