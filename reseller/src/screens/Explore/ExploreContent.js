import React, { Component } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Dimensions
} from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import LinearGradient from 'react-native-linear-gradient'
import Carousel, { Pagination } from 'react-native-snap-carousel'
import { SectionGrid } from 'react-native-super-grid'

import FontAwesome from 'react-native-vector-icons/FontAwesome'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import Ionicons from 'react-native-vector-icons/Ionicons'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'

import PerformanceCard from '../../components/global/Card/PerformanceCard'
import ProductSwiper from '../../components/global/Swiper/ProductSwiper'
import CategoryCard from '../../components/global/Card/CategoryCard'
import Images from '../../components/global/Images'
import { Theme } from '../../constants'

import blueJersey from '../../assets/images/blue-jersey.jpg'
import blueJeans from '../../assets/images/blue-jeans.jpg'
import blackJacket from '../../assets/images/black-jacket.jpg'
import blackMockupTshirt from '../../assets/images/black-mockup-tshirt.jpg'
import todayTshirt from '../../assets/images/today-tshirt.jpg'
import bestSeller from '../../assets/images/best-seller.jpg'
import baby from '../../assets/images/baby.jpg'
import men from '../../assets/images/men.jpg'
import flashSale from '../../assets/images/flash-sale.jpg'
import accessories from '../../assets/images/bracelet.jpg'
import blackFriday from '../../assets/images/black-friday.jpg'
import bigSale from '../../assets/images/big-sale.jpg'

class ExploreContent extends Component {
  state = {
    dataStore: [
      {
        title: 'Estimasi Keuntungan Bulan ini',
        amount: '3.333.000.000',
        percentage: '30'
      },
      {
        title: 'Teman Berhasil Diundang',
        amount: '30',
        percentage: '30'
      },
      {
        title: 'Pesanan Bulan ini',
        amount: '300',
        percentage: '30'
      },
      {
        title: 'Estimasi Keuntungan Rata-rata',
        amount: '300000',
        percentage: '30'
      },
      {
        title: 'Pesanan Bulan ini',
        amount: '300',
        percentage: '30'
      }
    ],
    dataBanner: [
      {
        images: blackFriday
      },
      {
        images: bigSale
      }
    ],
    dataNewProducts: [
      {
        images: blackJacket,
        name: 'Jaket Hitam',
        price: '323.000',
        express: true,
        soldout: '1.000'
      },
      {
        images: blueJersey,
        name: 'Jersey Biru',
        price: '120.000',
        express: true,
        soldout: '100'
      },
      {
        images: blueJeans,
        name: 'Jeans Biru',
        price: '150.000',
        express: false,
        soldout: '10'
      },
      {
        images: blackMockupTshirt,
        name: 'Baju Mockup Hitam',
        price: '50.000',
        express: false,
        soldout: '69'
      },
      {
        images: todayTshirt,
        name: 'Baju Today Biru',
        price: '100.000',
        express: false,
        soldout: '1'
      }
    ],
    dataCategory: [
      {
        title: 'Koleksi Terpopuler',
        data: [
          {
            category: 'Terlaris',
            images: bestSeller
          },
          {
            category: 'Bayi',
            images: baby
          },
          {
            category: 'Aksesoris',
            images: accessories
          },
          {
            category: 'Pria',
            images: men
          }
        ]
      }
    ],
    activeBanner: 0
  }

  render() {
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        bounces={false}
        data={[0]}
        keyExtractor={(item => item.toString())}
        renderItem={() => (
          <View>
            {this.topContent()}
            {this.midContent()}
            {this.bottomContent()}
          </View>
        )}
      />
    )
  }

  topContent = () => {
    return (
      <View style={styles['performance']}>
        {this.performanceHeader()}
        {this.performanceList()}
      </View>
    )
  }

  performanceHeader = () => {
    return (
      <View style={styles['performance__title__wrapper']}>
        {this.performanceTitle()}
        {this.performanceTooltip()}
      </View>
    )
  }

  performanceTitle = () => {
    return (
      <Text style={styles['performance__title']}>
        PERFORMA BISNIS
      </Text>
    )
  }

  performanceTooltip = () => {
    return (
      <TouchableOpacity
        onPress={() => {}}
        style={styles['performance__tip__btn']}
      >
        <Text style={styles['performance__tip__icon']}>
          i
        </Text>
      </TouchableOpacity>
    )
  }

  performanceList = () => {
    const { dataStore } = this.state

    return (
      <FlatList
        data={dataStore}
        horizontal
        showsHorizontalScrollIndicator={false}
        keyExtractor={(item, i) => item + i.toString()}
        renderItem={this.performanceCard}
      />
    )
  }

  performanceCard = ({ item, index }) => {
    return (
      <PerformanceCard
        icon={this.performanceCardIcon(index)}
        title={item.title}
        amount={item.amount}
        percentage={item.percentage}
        containerStyle={this.performanceCardStyle(index)}
      />
    )
  }

  performanceCardIcon = (index) => {
    let icon

    if (index === 0) {
      icon = (
        <View style={styles['card__icon__currency']}>
          <FontAwesome
            name='dollar'
            size={EStyleSheet.value('14rem')}
            color={Theme.Colors.LightPink}
          />
        </View>
      )
    } else if (index === 1) {
      icon = (
        <View style={styles['card__icon__invite']}>
          <FontAwesome5
            name='user-plus'
            size={EStyleSheet.value('19rem')}
            color={Theme.Colors.White}
          />
        </View>
      )
    } else if (index === 2 || index === 4) {
      icon = (
        <View style={styles['card__icon__basket']}>
          <Ionicons
            name='ios-basket'
            size={EStyleSheet.value('27rem')}
            color={Theme.Colors.White}
          />
        </View>
      )
    } else if (index === 3) {
      icon = (
        <SimpleLineIcons
          name='graph'
          size={EStyleSheet.value('24rem')}
          color={Theme.Colors.White}
        />
      )
    }

    return (
      <LinearGradient
        colors={[
          Theme.Colors.RedRuber,
          Theme.Colors.PinkOrange
        ]}
        style={styles['card__icon__wrapper']}
      >
        {icon}
      </LinearGradient>
    )
  }

  performanceCardStyle = (index) => {
    if (this.state.dataStore.length === index + 1) {
      return { marginRight: EStyleSheet.value('15rem') }
    }
  }

  midContent = () => {
    return (
      <View style={styles['main-content']}>
        {this.bannerSwiper()}
        {this.bannerInfo()}
        {this.products()}
      </View>
    )
  }

  bannerSwiper = () => {
    return (
      <View>
        {this.bannerList()}
        {this.bannerPagination()}
      </View>
    )
  }

  bannerList = () => {
    const { dataBanner } = this.state
    const itemWidth = Dimensions.get('window').width - EStyleSheet.value('33rem')

    return (
      <Carousel
        ref={(c) => { this._carousel = c }}
        data={dataBanner}
        inactiveSlideScale={0.95}
        inactiveSlideOpacity={1}
        sliderHeight={EStyleSheet.value('130rem')}
        itemHeight={EStyleSheet.value('130rem')}
        sliderWidth={Dimensions.get('window').width}
        itemWidth={itemWidth}
        renderItem={this.bannerListItem}
        onSnapToItem={(index) => this.setState({ activeBanner: index })}
        contentContainerCustomStyle={{ padding: EStyleSheet.value('10rem') }}
      />
    )
  }

  bannerListItem = ({ item, index }) => {
    return (
      <View style={styles['banner-list__wrapper']}>
        <Images
          key={index}
          source={item.images}
          resizeMode='cover'
          style={{
            height: EStyleSheet.value('100%'),
            width: EStyleSheet.value('100%')
          }}
        />
      </View>
    )
  }

  bannerPagination () {
    return (
      <Pagination
        dotsLength={this.state.dataBanner.length}
        activeDotIndex={this.state.activeBanner}
        containerStyle={styles['banner-list__pagination']}
        dotContainerStyle={styles['banner-list__pagination__dot__container']}
        dotStyle={styles['banner-list__pagination__dot']}
        inactiveDotStyle={{ marginHorizontal: 0 }}
        inactiveDotOpacity={0.5}
        inactiveDotScale={1}
      />
    )
  }

  bannerInfo = () => {
    return (
      <View style={styles['banner-info__wrapper']}>
        <View style={styles['banner-info']}>
          <Images
            source={flashSale}
            resizeMode='cover'
            style={{
              height: EStyleSheet.value('100%'),
              width: EStyleSheet.value('100%')
            }}
          />
        </View>
      </View>
    )
  }

  products = () => {
    return (
      <View>
        <ProductSwiper
          data={this.state.dataNewProducts}
          headerTitle='Terbaru'
        />
        <ProductSwiper
          data={this.state.dataNewProducts}
          headerTitle='ExpressDelivery'
          containerStyle={styles['products-express']}
        />
      </View>
    )
  }

  bottomContent = () => {
    return (
      <SectionGrid
        spacing={17}
        itemDimension={130}
        sections={this.state.dataCategory}
        renderItem={this.categoryList}
        renderSectionHeader={this.categoryTitle}
        style={{ marginBottom: EStyleSheet.value('35rem') }}
      />
    )
  }

  categoryTitle = ({ section }) => {
    return (
      <Text style={styles['category__title']}>
        {section.title}
      </Text>
    )
  }

  categoryList = ({ item, index }) => {
    return (
      <CategoryCard
        key={index}
        images={item.images}
        title={item.category}
        height={EStyleSheet.value('75rem')}
        width={EStyleSheet.value('145rem')}
      />
    )
  }
}

export default ExploreContent

const styles = EStyleSheet.create({
  'performance': {
    backgroundColor: Theme.Colors.RedRuber
  },
  'performance__title__wrapper': {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: '15rem',
    paddingBottom: '5rem'
  },
  'performance__title': {
    fontSize: '14rem',
    color: Theme.Colors.White,
    fontWeight: '700',
    marginLeft: 'auto',
    paddingLeft: '30rem'
  },
  'performance__tip__btn': {
    borderRadius: '10rem',
    width: '11rem',
    height: '11rem',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Theme.Colors.White,
    marginLeft: 'auto',
    marginRight: '15rem'
  },
  'performance__tip__icon': {
    fontSize: '8rem',
    color: Theme.Colors.RedRuber
  },
  'card__icon__wrapper': {
    borderRadius: '100rem',
    alignSelf: 'flex-start',
    alignItems: 'center',
    justifyContent: 'center',
    height: '40rem',
    width: '40rem'
  },
  'card__icon__currency': {
    borderRadius: '100rem',
    paddingVertical: '5.5rem',
    paddingHorizontal: '9rem',
    backgroundColor: Theme.Colors.White
  },
  'card__icon__invite': {
    marginBottom: '2rem'
  },
  'card__icon__basket': {
    marginTop: '2rem'
  },
  'banner-list__pagination__container': {
    justifyContent: 'flex-start',
    marginVertical: '0rem',
    marginBottom: '25rem',
    paddingLeft: '28rem'
  },
  'banner-list__pagination__item': {
    height: '6rem',
    width: '6rem',
    marginHorizontal: '3rem'
  },
  'main-content': {
    backgroundColor: Theme.Colors.Silver
  },
  'banner-list__wrapper': {
    borderRadius: '8rem',
    overflow: 'hidden',
    backgroundColor: Theme.Colors.PinkOrange,
    height: '110rem'
  },
  'banner-list__pagination': {
    justifyContent: 'flex-start',
    paddingHorizontal: '0rem',
    paddingVertical: '0rem',
    position: 'absolute',
    bottom: '23rem',
    left: '30rem'
  },
  'banner-list__pagination__dot__container': {
    marginHorizontal: '2rem'
  },
  'banner-list__pagination__dot': {
    width: '6rem',
    height: '6rem',
    borderRadius: '6rem',
    marginHorizontal: '0rem',
    backgroundColor: Theme.Colors.White
  },
  'banner-info__wrapper': {
    padding: '15rem',
    paddingVertical: '5rem'
  },
  'banner-info': {
    borderRadius: '8rem',
    overflow: 'hidden',
    height: '80rem',
    backgroundColor: Theme.Colors.LightPink,
  },
  'products-express': {
    marginTop: '8rem',
    marginBottom: '15rem'
  },
  'category': {
    backgroundColor: Theme.Colors.White,
    paddingVertical: '10rem',
    paddingHorizontal: '15rem'
  },
  'category__title': {
    fontSize: '14rem',
    fontWeight: '700',
    color: Theme.Colors.PinkOrange,
    paddingHorizontal: '15rem',
    paddingTop: '7rem',
    paddingBottom: '3rem'
  }
})