import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  View
} from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'

import ExploreContent from './ExploreContent'
import MainHeader from '../../components/global/Header/MainHeader'
import { Theme } from '../../constants'

class ExploreScreen extends Component {
  render() {
    return (
      <View style={styles['container']}>
        <MainHeader />
        <ExploreContent />
      </View>
    )
  }
}

ExploreScreen.propTypes = {
  navigation: PropTypes.object
}

export default ExploreScreen

const styles = EStyleSheet.create({
  'container': {
    flex: 1,
    backgroundColor: Theme.Colors.White
  }
})