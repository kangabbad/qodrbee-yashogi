import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  StatusBar,
  Platform,
  TouchableWithoutFeedback,
  View
} from 'react-native'
import {
  SearchBar,
  Button
} from 'react-native-elements'
import { TransitionPresets } from 'react-navigation-stack'
import { SafeAreaView } from 'react-native-safe-area-context'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import EStyleSheet from 'react-native-extended-stylesheet'

import { Theme } from '../constants'

class SearchScreen extends Component {
  static navigationOptions = {
    ...TransitionPresets.FadeFromBottomAndroid,
    transitionSpec: {
      open: {
        animation: 'timing',
        config: { duration: 100 }
      },
      close: {
        animation: 'timing',
        config: { duration: 100 }
      }
    }
  }

  statusBar = () => {
    return (
      <StatusBar
        barStyle='dark-content'
        backgroundColor='rgba(0, 0, 0, .005)'
      />
    )
  }

  searchBar = () => {
    return (
      <View style={styles['searchbar__wrapper']}>
        <SearchBar
          platform={this.searchBarPlatform()}
          autoFocus
          showCancel
          placeholder='Cari...'
          searchIcon={this.searchIconPlatform()}
          cancelIcon={this.backBtn()}
          onCancel={this.onCancelSearchBar}
          containerStyle={styles['searchbar__container']}
          inputContainerStyle={styles['searchbar__input__container']}
          inputStyle={styles['searchbar__input']}
          leftIconContainerStyle={Platform.select({ android: { marginLeft: 0 }, ios: {} })}
          cancelButtonProps={{
            buttonTextStyle: {
              paddingRight: 0
            }
          }}
        />
        <Button
          title='Back'
          onPress={this.onCancelSearchBar}
        />
      </View>
    )
  }

  searchBarPlatform = () => {
    return Platform.select({
      android: 'android',
      ios: 'ios'
    })
  }

  searchIconPlatform = () => {
    return Platform.OS === 'android'
      ? this.backBtn()
      : this.searchIcon()
  }

  onCancelSearchBar = () => {
    this.props.navigation.goBack()
  }

  backBtn = () => {
    return (
      <TouchableWithoutFeedback
        onPress={this.onCancelSearchBar}
      >
        <MaterialCommunityIcons
          name='arrow-left'
          size={EStyleSheet.value('24rem')}
          color={Theme.Colors.Black}
        />
      </TouchableWithoutFeedback>
    )
  }

  searchIcon = () => {
    return (
      <MaterialIcons
        name='search'
        size={EStyleSheet.value('18rem')}
        color={Theme.Colors.Black}
      />
    )
  }
  
  render() {
    return (
      <SafeAreaView>
        {this.statusBar()}
        {this.searchBar()}
      </SafeAreaView>
    )
  }
}

SearchScreen.propTypes = {
  navigation: PropTypes.object
}

export default SearchScreen

const styles = EStyleSheet.create({
  'searchbar__wrapper': {
    backgroundColor: Theme.Colors.White,
    paddingHorizontal: '13rem',
    
  },
  'searchbar__container': {
    backgroundColor: Theme.Colors.White
  },
  'searchbar__input__container': {
    marginLeft: '0rem'
  },
  'searchbar__input': {
    marginLeft: '15rem',
    marginRight: '0rem'
  }
})
