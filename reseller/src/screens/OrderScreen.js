import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { 
  View,
  Text
} from 'react-native'
// import { NavigationActions, StackActions } from 'react-navigation'

// const resetStackNavigation = StackActions.reset({
//   index: 0,
//   actions: [NavigationActions.navigate({ routeName: 'HomeScreen' })]
// })

class OrderScreen extends Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text style={{ marginBottom: 15 }}>Order Screen</Text>
      </View>
    )
  }
}

OrderScreen.propTypes = {
  navigation: PropTypes.object
}

export default OrderScreen