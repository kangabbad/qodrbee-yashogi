import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Text,
  TouchableHighlight,
  TouchableNativeFeedback,
  TouchableOpacity,
  Image,
  ImageBackground,
  Slider
} from 'react-native'

class AccountScreen extends Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text style={{ marginBottom: 15 }}>Account Screen</Text>
      </View>
    )
  }
}

AccountScreen.propTypes = {
  navigation: PropTypes.object
}

export default AccountScreen